# @field DEVICENAME
# @type STRING
# Device name, should be the same as the name in CCDB


# EtherCAT scanner configuration
# ecat2configure(<domainnr>, <frequency>, <autoconfig>, <autostart>)
ecat2configure(0,1000,1,1)

# Load database for every module installed in this I/O node
dbLoadRecords("ecat2ek1101.db", "PREFIX=$(DEVICENAME), MOD_ID=COUPLER, SLAVE_IDX=0")
dbLoadRecords("ecat2el3164.db", "PREFIX=$(DEVICENAME), MOD_ID=AIMOD1, SLAVE_IDX=1")
dbLoadRecords("ecat2el3164.db", "PREFIX=$(DEVICENAME), MOD_ID=AIMOD2, SLAVE_IDX=2")
#dbLoadRecords("ecat2el3164.db", "PREFIX=$(DEVICENAME), MOD_ID=AIMOD3, SLAVE_IDX=3")

# Load the database defining your EPICS records
dbLoadRecords("lebt-010_vac-ecatio-10001.db", "DEVICENAME=$(DEVICENAME)")

#- Increase buffer size. In theory 320000 should be enough, but it isn't. CS-Studio complains
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "420000")

# EtherCAT I/O Station

EPICS module to provide communications to/from EtherCAT I/O station.

* This is a module specific to a particular EtherCAT I/O station.
* This module contains the database with records that EPICS will read/write to this EtherCAT I/O station.

If you need to create a new module for EPICS to communicate to an EtherCAT I/O station:

1. Create a copy of this module
2. Name accordingly
3. Modify startup script to match your EtherCAT I/O station configuration (what kind and how many I/O modules)
4. Modify database

## Startup Examples

`iocsh -r lebt-010_vac-ecatio-10001,1.0.0 -c 'requireSnippet(lebt-010_vac-ecatio-10001.cmd, "DEVICENAME=LEBT-010:Vac-ECATIO-10001")'

If ran as proper ioc service:
```
epicsEnvSet(DEVICENAME, "LEBT-010:Vac-ECATIO-10001")
require lebt-010_vac-ecatio-10001, 1.0.0
< ${REQUIRE_lebt-010_vac-ecatio-10001_PATH}/startup/lebt-010_vac-ecatio-10001.cmd
```
